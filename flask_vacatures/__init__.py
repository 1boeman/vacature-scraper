import os
from flask import Flask



def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        MYSQL_USER='vagrant',
        MYSQL_PASSWORD='password',
        MYSQL_DB='vacature_db',
        COLUMNS = [
            [{"key" : 'uva', "titel":"UvA"},{"key":"culturele","titel":"Culturele Vacatures"},
                {"key":"hva","titel":"HvA"},
                {"key":"museumvereniging","titel":"Museumvereniging"}],
            [{"key":"amsterdam","titel":"Amsterdam.nl"},{"key":'surf',"titel":"Surf.nl"},{"key":'goededoelen', "titel":"GoedeDoelen.nl"},
                {"key":"oneworld","titel":"Oneworld.nl"},{"key":"fondsen", "titel": "Fondsen.org"},{"key":"vluchtelingenwerk","titel":"Vluchtelingenwerk"}],
            [{"key":"academictransfer","titel":"AcademicTransfer.com"},
                {"key":"werkenbijhogescholen","titel":"WerkenBijHogescholen.nl"},{"key":"werkenvoornederland","titel":"Werkenvoornederland.nl"},{"key":"tweakers","titel":"Tweakers.nl"},
                    {"key":"indeed_bieb","titel":"Indeed.nl Bieb A'dam"}, 
                    {"key":"indeed_dig_hum","titel":"Indeed.nl Digital Humanities"},{"key":"indeed_python","titel":"Indeed.nl Python"}]
        ],
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    db.init_app(app)

    from . import index
    app.register_blueprint(index.bp)

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    return app
