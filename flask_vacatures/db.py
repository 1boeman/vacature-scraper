import mysql.connector as database
from flask import current_app,g



def init_app(app):
    app.teardown_appcontext(close_db)



def get_db():
    if 'db' not in g:
        cnx = database.connect(
            user=current_app.config['MYSQL_USER'],
            password=current_app.config['MYSQL_PASSWORD'],
            host='localhost',
            database=current_app.config['MYSQL_DB'],
        )
        g.db = cnx
    return g.db



def get_cursor():
    db = get_db()
    return db.cursor(dictionary=True)



def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()
