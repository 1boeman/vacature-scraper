from flask import Blueprint, render_template,current_app
from . import db 


bp=Blueprint('index', __name__)

@bp.route('/')
def home():
    cursor = db.get_cursor() 
    q = ('select v.*,p.plaatsnaam,o.titel as titel_organisatie from vacature v \
            left join vacature_has_plaats vp on v.id_vacature = vp.id_vacature \
                left join plaats p on p.id_plaats = vp.id_plaats \
                left join organisatie o on o.id_organisatie = v.id_organisatie \
                order by v.bron, v.sort_order asc')
    cursor.execute(q)
    r = cursor.fetchall()
    c = current_app.config['COLUMNS']
    
    return render_template('home.html', columns=c, rows=r )

