var clickHandlers = {
    "expand_list":function(){
        console.log(this.parentNode)
        this.parentNode.classList.add('expanded')        

    },
    "vacature_titel":function(){
        if (this.classList.contains('open')){
            this.classList.remove('open')
            this.parentNode.querySelectorAll('.opened').forEach(function(item,i){
                item.classList.remove("opened")
                item.classList.add("d-none")
            })

        } else { 
            this.classList.add('open')
            this.parentNode.querySelectorAll('.d-none').forEach(function(item,i){
                item.classList.remove("d-none")
                item.classList.add("opened")
            })
        }
    }
}
