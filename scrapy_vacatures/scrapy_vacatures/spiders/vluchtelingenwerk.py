import scrapy


class VluchtelingenwerkSpider(scrapy.Spider):
    sort_order = 0
    name = 'vluchtelingenwerk'
    allowed_domains = ['www.vluchtelingenwerk.nl']
    start_urls = ['https://www.vluchtelingenwerk.nl/vacatures?field_type_vacature_value=betaald&field_gemeente_vac_ref_tid=']



    def parse(self, response):
        for v in response.css('.view-content .item-list li'):
            self.sort_order += 1
            detail_url = v.xpath('.//a[1]/@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))


    def parse_detailpage(self,response,item):
        
        yield {
            "titel":response.xpath('//h1[1]/text()').get(),
            'salaris_indicatie':'',
            'opleiding':'',
            "omvang":"",
            "vakgebied":"",
            "organisatie":"Vluchtelingenwerk",
            "plaatsnaam":"",
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath("//div[contains(@class,'content')]//text()").getall()),
        } 
