import scrapy



class SurfSpider(scrapy.Spider):
    sort_order = 0
    name = 'surf'
    allowed_domains = ['surf.nl']
    start_urls = ['https://www.surf.nl/over-surf/vacatures-bij-surf']

    def parse(self, response):
        for l in response.css('.item-list'):
            plaats = l.css('h2::text').get() 
            if plaats is not None:
                for v in l.css('.node--type-vacancy'):
                    self.sort_order += 1
                    detail_url = v.xpath('.//a/@href').get()
                    detail_url = response.urljoin(detail_url)
                    i = {
                        "link":detail_url,
                        'plaats':plaats,
                        "beschrijving":v.css('.field--name-field-teaser::text').get(),
                        "sort_order":str(self.sort_order),
                    }
                    
                    yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))



    def parse_detailpage(self, response, item):

        b = response.xpath('//div[contains(@class,"field--name-field-components")]//text()').getall()
        b.insert(0,item['beschrijving'])
        spec_list = response.css('.node-main-components .paragraph--view-mode--main .field__item ul')
        opl = spec_list.xpath('/li[1]/text()').get()
        omvang =  spec_list.xpath("/li[starts-with(text(),'Aantal uur')]/text()").get()
  #      sluit =  spec_list.xpath("/li[starts-with(text(),'Sluitingsdatum')]/text()").get()
        salaris = response.xpath("//li[contains(text(),'salaris')]//text()").get()


        yield {
            "titel":response.xpath("//h1[contains(@class, 'page-title')]//text()").get(),
            'salaris_indicatie':salaris,
            'omvang':omvang,
            'vakgebied':'onderwijs',
#            'datum_pub':'',
 #           'datum_sluit':sluit,
            'organisatie':'Surf',
            "plaatsnaam":item['plaats'],
            "land":'NL',
            "beschrijving":" -- ".join(b),
            "opleiding":opl,
            "link":item["link"], 
            'sort_order':item['sort_order'],
        }
