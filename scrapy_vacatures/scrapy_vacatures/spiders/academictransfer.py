import scrapy


class AcademictransferSpider(scrapy.Spider):
    sort_order = 0
    name = 'academictransfer'
    allowed_domains = ['www.academictransfer.com']
    start_urls = [  'https://www.academictransfer.com/en/?vacancy_type=non_scientific&q=Amsterdam&order=published&page=1',
                    'https://www.academictransfer.com/en/?vacancy_type=scientific&q=Amsterdam&order=published&page=1',
                    'https://www.academictransfer.com/en/?vacancy_type=non_scientific&q=Amsterdam&order=published&page=2',
                    'https://www.academictransfer.com/en/?vacancy_type=scientific&q=Amsterdam&order=published&page=2',
    ]
    def parse(self, response):
        for a in response.xpath("//a[@data-interaction='vacancy']"):
            self.sort_order += 1
            detail_url = a.xpath('./@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
 
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))


    def parse_detailpage(self,response,item):
        plaatsnaam = response.xpath('//td[@data-label="Location"]/text()').get()
        if not plaatsnaam:
            plaatsnaam = response.xpath('//td[@data-label="Locatie"]/text()').get()

 
        yield {
            "titel":response.xpath('//h1[contains(@class,"Header")]/text()').get(),
            'salaris_indicatie':response.xpath('//li[contains(@class,"Specs_salary")]/span/text()').get(),
            'omvang':response.xpath('//li[contains(@class,"Specs_weekly_hours")]/span/text()').get(),
            'vakgebied':response.xpath('//li[contains(@class,"Specs_job_type")]/span/text()').get(),

            'organisatie':response.xpath('//p[contains(@class,"Employer_name")]/text()').get(),
            "plaatsnaam":plaatsnaam,
            "land":'',
            "beschrijving":" -- ".join(response.xpath('//div[contains(@class,"Main_content")]//text()').getall()),
            "opleiding":response.xpath('//p[contains(@class,"Specs_education_level")]/text()').get(),
            "link":item["link"], 
            'sort_order':item['sort_order'],
        }
