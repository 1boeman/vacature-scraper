import scrapy


class IndeedPythonSpider(scrapy.Spider):
    sort_order=0
    name = 'indeed_python'
    allowed_domains = ['nl.indeed.com']
    start_urls = ['https://nl.indeed.com/jobs?q=Python%20Programmeur&l=Amsterdam']


    def parse(self, response):
        for v in response.css('a.result'):
            self.sort_order += 1
            detail_url = v.xpath('./@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
                "salaris_indicatie":v.css('.salary-snippet::text').get(),
                "organisatie":v.css('.companyName::text').get(),
                "plaatsnaam":v.css('.companyLocation::text').get(),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))


    def parse_detailpage(self,response,item):
        
        yield {
            "titel":" ".join( response.xpath('//h1[contains(@class,"jobsearch-JobInfoHeader-title")]//text()').getall()),
            'salaris_indicatie':item['salaris_indicatie'],
            'opleiding':'',
            "omvang":"",
            "vakgebied":'',
            "organisatie":item["organisatie"],
            "plaatsnaam":item["plaatsnaam"],
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath('//div[contains(@class,"jobsearch-ViewJobLayout-jobDisplay")]//text()').getall()),
        }
