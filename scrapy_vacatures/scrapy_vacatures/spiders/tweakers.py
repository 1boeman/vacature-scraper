import scrapy
from scrapy_selenium import SeleniumRequest


class TweakersSpider(scrapy.Spider):
    sort_order=0
    name = 'tweakers'
    allowed_domains = ['tweakers.nl','tweakers.net']
    
    
    def start_requests(self):
        urls = ["https://tweakers.net/carriere/it-banen/zoeken/#filter:q1bKzEspLS4pykwtVrKKNjTRMTfRMTTQsYjVUcrJT04syczPU7JSMjQwNQzyVdJRyi9KSS1yy0zNSQGKlmTmpirVAgA"]
        for url in urls:
            yield SeleniumRequest(url=url,wait_time=3, callback=self.parse_result)



    def parse_result(self, response):
        driver = response.request.meta['driver']
        button = driver.find_element_by_css_selector('#cookieAcceptForm button.ctaButton')
        button.click()
        driver.implicitly_wait(4) 
        jobs = driver.find_elements_by_css_selector('.jobItem a.jobTitle')
        for j in jobs:
            detail_url = j.get_attribute('href')
            print ('****')
            print (detail_url)
            detail_url = response.urljoin(detail_url)
 
            self.sort_order +=1
            i = {
               'link':detail_url,
               'sort_order':str(self.sort_order),
            } 
            print (i)
            yield scrapy.Request(detail_url, callback=self.parse_detailpage,cb_kwargs=dict(item = i))



    def parse_detailpage(self,response,item):
        yield {
            "titel":response.xpath('//h1[1]/text()').get(),
            'salaris_indicatie':response.xpath('//p[contains(text(),"€")]/text()').get(),
            'opleiding':response.xpath('//b[contains(text(),"Opleidingsniveau")]/following-sibling::span/text()').get(),
            "omvang":response.xpath('//b[contains(text(),"Uren")]/following-sibling::span/text()').get(),
            "vakgebied":response.xpath('//b[contains(text(),"Functiegroep")]/following-sibling::span/text()').get(),
            "organisatie":response.xpath('//div[contains(@class,"jobCompany")]//h2[1]/text()').get(),
            "plaatsnaam":response.xpath('//b[contains(text(),"Werklocatie")]/following-sibling::span/text()').get(),
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath('//div[contains(@id,"contentArea")]//text()').getall()),
        } 
