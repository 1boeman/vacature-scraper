import scrapy


class HvaSpider(scrapy.Spider):
    sort_order=0
    name = 'hva'
    allowed_domains = ['vacaturesuvahva.nl']
    start_urls = ['https://vacaturesuvahva.nl/HvA/search/']

    def parse(self, response):
        for v in response.css('tr.job-tile'):
            self.sort_order += 1
            detail_url = v.xpath('.//a[1]/@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))



    def parse_detailpage(self,response,item):
        
        yield {
            "titel":" ".join( response.xpath('//h1[1]//text()').getall()),
            'salaris_indicatie':response.xpath('//li[contains(text(),"€")]/text()').get(),
            'opleiding':'',
            "omvang":"",
            "vakgebied":'',
            "organisatie":"Hogeschool van Amsterdam",
            "plaatsnaam":"Amsterdam",
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath('//div[contains(@class,"jobDisplay")]//text()').getall()),
        }
