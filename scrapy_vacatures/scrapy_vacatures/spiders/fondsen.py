import scrapy


class FondsenSpider(scrapy.Spider):
    sort_order=0
    name = 'fondsen'
    allowed_domains = ['www.fondsen.org']
    start_urls = ['https://www.fondsen.org/professionals/',
                    'https://www.fondsen.org/professionals/2/']

    def parse(self, response):
        for v in response.css('.type-vacature'):
            self.sort_order += 1
            detail_url = v.xpath('.//h3/a/@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))


    def parse_detailpage(self,response,item):
        
        yield {
            "titel":response.xpath('//h1/text()').get(),
            'salaris_indicatie':response.xpath('//li[contains(text(),"€")]/text()').get(),
            'opleiding':'',
            "omvang":response.xpath('//div/h6[contains(text(),"Contract")]/../../following-sibling::div//h6//text()').get(),
            "vakgebied":response.xpath('//div/h6[contains(text(),"Functiegroep")]/../../following-sibling::div//h6//text()').get(),

            "organisatie":response.xpath("//h2/text()").get(),
            "plaatsnaam":response.xpath('//div/h6[contains(text(),"Locatie")]/../../following-sibling::div//h6//text()').get(),
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath('//div[contains(@class,"elementor-section-wrap")]/section[3]//text()').getall()),
        } 
