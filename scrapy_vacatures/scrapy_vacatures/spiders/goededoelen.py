import scrapy


class GoededoelenSpider(scrapy.Spider):
    sort_order = 0
    name = 'goededoelen'
    allowed_domains = ['goededoelen.nl']
    start_urls = ['https://goededoelen.nl/vacatures/betaald','https://goededoelen.nl/vacatures/betaald/p2',
        'https://goededoelen.nl/vacatures/betaald/p3']

    def parse(self, response):
        for v in response.css('.block__link'):
            self.sort_order += 1
            detail_url = v.xpath('./@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))

    def parse_detailpage(self,response,item):
        
        yield {
            "titel":response.xpath('//h1/text()').get(),
            'salaris_indicatie':response.xpath('//li[contains(text(),"€")]/text()').get(),
            'opleiding':'',
            "omvang":response.xpath('//div[@class="table-flex__item"]/strong[contains(text(),"Aantal uren")]/parent::div/text()').get(),
            "vakgebied":"",

            "organisatie":response.xpath('//div[@class="table-flex__item"]/strong[contains(text(),"Organisatie")]/parent::div/text()').get(),
            "plaatsnaam":response.xpath('//div[@class="table-flex__item"]/strong[contains(text(),"Plaats")]/parent::div/text()').get(),
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath("//article[contains(@class,'article-content')]//text()").getall()),
        } 
