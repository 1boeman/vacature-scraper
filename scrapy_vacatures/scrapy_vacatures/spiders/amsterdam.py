import scrapy


class AmsterdamSpider(scrapy.Spider):
    sort_order=0
    name = 'amsterdam'
    allowed_domains = ['www.amsterdam.nl']
    start_urls = ['https://www.amsterdam.nl/bestuur-en-organisatie/werkenbij/externe/']

    def parse(self, response):
        for v in response.css('.resultaat'):
            self.sort_order += 1
            detail_url = v.xpath('.//a[1]/@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))


   
    def parse_detailpage(self,response,item):
        
        yield {
            "titel":response.xpath('//h1[1]/text()').get(),
            'salaris_indicatie':response.xpath('//dd/div[contains(text(),"€")]/text()').get(),
            'opleiding':response.xpath("//dd/div[contains(@class,'werk-/denkniveau')]/text()").get(),
            "omvang":response.xpath("//dd/div[contains(@class,'uren_per_week')]/text()").get(),
            "vakgebied":response.xpath("//dd/div[contains(@class,'vakgebied')]/text()").get(),
            "organisatie":"Gemeente Amsterdam",
            "plaatsnaam":"Amsterdam",
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath('//div[contains(@class,"vacature_holder")]//text()').getall()),
        }
