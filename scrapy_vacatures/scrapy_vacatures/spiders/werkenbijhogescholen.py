import scrapy


class WerkenbijhogescholenSpider(scrapy.Spider):
    sort_order = 0
    name = 'werkenbijhogescholen'
    allowed_domains = ['www.werkenbijhogescholen.nl']
    start_urls = ['https://www.werkenbijhogescholen.nl/vacatures/?location=Amsterdam&location_range=25']

    def parse(self, response):
        for v in response.css('.panel-vacancy'):
            self.sort_order += 1
            detail_url = v.xpath('.//h2/a/@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
 
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))


    def parse_detailpage(self,response,item):
        s = response.xpath('//dt[starts-with(text(),"Salari")]/following-sibling::dd/text()').get() 
        o = response.xpath('//dt[contains(text(),"Opleidingsniveau")]/following-sibling::dd/text()').get() 
        omv = response.xpath('//dt[contains(text(),"Aanstellingsomvang")]/following-sibling::dd/text()').get() 
        vak = response.xpath('//dt[contains(text(),"Opleidingssector")]/following-sibling::dd/text()').get() 
        org = response.xpath('//dt[contains(text(),"Organisatie")]/following-sibling::dd/text()').get() 
        plaats = response.xpath('//dt[contains(text(),"Standplaats")]/following-sibling::dd/text()').get() 
        plaats = plaats.split().pop()
        yield {
            "titel":response.xpath('//h1/text()').get(),
            'salaris_indicatie':s,
            'opleiding':o,
            "omvang":omv,
            "vakgebied":vak,
            "organisatie":org,
            "beschrijving":" -- ".join(response.xpath("//div[contains(@class,'col-description')]//text()").getall()),
            "plaatsnaam":plaats, 
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
        } 
