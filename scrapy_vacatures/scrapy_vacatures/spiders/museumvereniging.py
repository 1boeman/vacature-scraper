import scrapy


class MuseumverenigingSpider(scrapy.Spider):
    sort_order = 0
    name = 'museumvereniging'
    allowed_domains = ['www.museumvereniging.nl']
    start_urls = ['https://www.museumvereniging.nl/vacatures',
                    'https://www.museumvereniging.nl/vacatures/page=1',
                        'https://www.museumvereniging.nl/vacatures/page=2']

    def parse(self, response):
        for v in response.xpath('//a[contains(@class,"u-width--100")]/div[.//h3]/parent::a'):
            self.sort_order += 1
            detail_url = v.xpath('.//@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))

    def parse_detailpage(self,response,item):
        
        specs = response.xpath('//div[contains(@class,"s-cms-content--introduction")]/p/br/following-sibling::text()').getall()
        yield {
            "titel":response.xpath('//h1[1]/text()').get(),
            'salaris_indicatie':'',
            'opleiding':'',
            "organisatie":response.xpath('//div[contains(@class,"s-cms-content--introduction")]/p[contains(text(),"Organisatie")]/a/text()').get(),
            "vakgebied":"",
            "omvang":response.xpath('//div[contains(@class,"s-cms-content--introduction")]/p[contains(text(),"Organisatie")]//text()').get(),
            "plaatsnaam":specs[0].split(':')[1],
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath("//div[contains(@class,'s-cms-content')]//text()").getall()),
        } 
