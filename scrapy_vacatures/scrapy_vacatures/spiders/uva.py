import scrapy


class UvaSpider(scrapy.Spider):
    sort_order = 0
    name = 'uva'
    allowed_domains = ['uva.nl']
    start_urls = ['https://www.uva.nl/over-de-uva/werken-bij-de-uva/vacatures/vacatures.html']

    def parse(self, response):
        for v in response.css('.c-vacancy'):
            detail_url = v.css('a.c-item__link::attr(href)').get()
            detail_url = response.urljoin(detail_url)
            self.sort_order += 1
            i = {
                'link':detail_url,
                'sort_order':str(self.sort_order),
            #    'datum_pub':v.css('.c-item__data span.value::text')[0].get(),
            #    'datum_sluit':v.css('.c-item__data span.value::text')[1].get(),
            }
            yield scrapy.Request(detail_url, callback=self.parse_detailpage,cb_kwargs=dict(item = i))

        next_page = response.css('li.pagination-next a::attr(href)').get()
        if next_page is not None:
           next_page = response.urljoin(next_page)
           yield scrapy.Request(next_page, callback=self.parse)


    def parse_detailpage(self, response, item):
            
            salaris_indicatie = response.xpath("//tr[th[starts-with(text(),'Salar')]]/td/text()").get()
            opl_niv = response.xpath("//tr[th[starts-with(text(),'Opleidingsniv')]]/td/text()").get()
            if not opl_niv:
                opl_niv = response.xpath("//tr[th[starts-with(text(),'Level of ed')]]/td/text()").get()
            omvang =  response.xpath("//tr[th[starts-with(text(),'Functieomva')]]/td/text()").get()
            if not omvang:
                omvang =  response.xpath("//tr[th[starts-with(text(),'Hours')]]/td/text()").get()

            d =  response.xpath('//div[@class="richtext"]//text()').getall()


            yield {
                'titel':response.css('.c-card__title::text').get(),
                'salaris_indicatie':salaris_indicatie,
                'opleiding':opl_niv,
                'omvang':omvang,
                'link':item['link'],
                'beschrijving':" -- ".join(d),
                'organisatie':'Universiteit van Amsterdam',
                'vakgebied':'onderwijs', 
                'plaatsnaam':'Amsterdam',
                'land':'NL',
                'sort_order':item['sort_order'],
             #   'datum_pub':item['datum_pub'],
             #   'datum_sluit':item['datum_sluit'],
            }

