import scrapy


class OneworldSpider(scrapy.Spider):
    sort_order=0
    name = 'oneworld'
    allowed_domains = ['www.oneworld.nl']
    start_urls = ['https://www.oneworld.nl/?s=&contract=&education=&location=&time=&type=betaald-werk&post_type=job']

    def parse(self, response):
        for v in response.css('a.excerpt-link'):
            self.sort_order += 1
            detail_url = v.xpath('./@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))



    def parse_detailpage(self,response,item):
        
        yield {
            "titel":" ".join( response.xpath('//h1[contains(@class,"headline")]//text()').getall()),
            'salaris_indicatie':response.xpath('//p[contains(text(),"€")]/text()').get(),
            'opleiding':response.xpath('//dt[contains(text(),"Opleidingsniveau")]/following-sibling::dd/text()').get(),
            "omvang":response.xpath('//dt[contains(text(),"Tijd per week")]/following-sibling::dd/text()').get(),
            "vakgebied":'',
            "organisatie":response.xpath('//h2[contains(@class,"organisation-name")]/text()').get(),
            "plaatsnaam":response.xpath('//dt[contains(text(),"Locatie")]/following-sibling::dd/text()').get(),
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath('//main[contains(@class,"content")]//text()').getall()),
        }
