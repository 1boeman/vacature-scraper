import scrapy
from scrapy_selenium import SeleniumRequest
from time import sleep

class WerkenvoornederlandSpider(scrapy.Spider):
    sort_order=0
    name = 'werkenvoornederland'
    allowed_domains = ['werkenvoornederland.nl']


    def start_requests(self):
        urls = ["https://www.werkenvoornederland.nl/vacatures?vakgebied=CVG.08%2CCVG.04%2CCVG.33%2CCVG.15%2CCVG.14&pagina=3"]
        for url in urls:
            yield SeleniumRequest(url=url,wait_time=10, callback=self.parse_result)


    def parse_result(self, response):
        sleep(8)
        driver = response.request.meta['driver']

        jobs = driver.find_elements_by_css_selector('.vacancy-list__item section.vacancy')
        for j in jobs:
            print('*****')
            detail_url = j.find_element_by_css_selector("a").get_attribute('href')
            print (detail_url)
            detail_url = response.urljoin(detail_url)
            self.sort_order +=1
            i = {
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url, callback=self.parse_detailpage,cb_kwargs=dict(item = i))

    def parse_detailpage(self,response,item):
        yield {
            "titel":response.xpath('//h1[1]/text()').get(),
            'salaris_indicatie':" - ".join(response.xpath('//span[contains(text(),"€")]/text()').getall()),
            'opleiding':'',
            "omvang":response.xpath('//span[contains(text(),"Uren")]/following-sibling::span/span/text()').get(),
            "vakgebied":response.xpath('//span[contains(text(),"Vakgebied")]/following-sibling::span/text()').get(),
            "organisatie":response.xpath('//p[contains(@class,"job-header__title")]/text()').get(),
            "plaatsnaam":response.xpath('//span[contains(text(),"Stand­plaats")]/following-sibling::span/text()').get(),
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath('//div[contains(@class,"job-container")]//text()').getall()),
        } 
