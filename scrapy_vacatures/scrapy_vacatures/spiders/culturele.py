import scrapy


class CultureleSpider(scrapy.Spider):
    sort_order=0
    name = 'culturele'
    allowed_domains = ['www.culturele-vacatures.nl']
    start_urls = ['https://www.culturele-vacatures.nl/vacatures-zoeken/?fwp_soort_vacature=betaalde-functie&fwp_provincie=noord-holland',
'https://www.culturele-vacatures.nl/vacatures-zoeken/?fwp_soort_vacature=betaalde-functie&fwp_provincie=noord-holland&fwp_paged=2',
'https://www.culturele-vacatures.nl/vacatures-zoeken/?fwp_soort_vacature=betaalde-functie&fwp_provincie=noord-holland&fwp_paged=3',
'https://www.culturele-vacatures.nl/vacatures-zoeken/?fwp_soort_vacature=betaalde-functie&fwp_provincie=noord-holland&fwp_paged=4',
    ]

    def parse(self, response):
        for v in response.css('.fwpl-result'):
            self.sort_order += 1
            detail_url = v.xpath('.//a/@href').get()
            detail_url = response.urljoin(detail_url)
            i ={
                "link":detail_url,
                "sort_order":str(self.sort_order),
            }
            yield scrapy.Request(detail_url,callback=self.parse_detailpage,cb_kwargs=dict(item=i))



    def parse_detailpage(self,response,item):
        
        yield {
            "titel":" ".join( response.xpath('//h1[contains(@class,"entry-title")]//text()').getall()),
            'salaris_indicatie':response.xpath('//li[contains(text(),"€")]/text()').get(),
            'opleiding':'',
            "omvang":response.xpath('//article/div[@data-first_letter]/p[1]/strong[4]/text()').get(),
            "vakgebied":'',
            "organisatie":response.xpath('//article/div[@data-first_letter]/p[1]/strong[1]/text()').get(),
            "plaatsnaam":response.xpath('//article/div[@data-first_letter]/p[1]/strong[2]/text()').get(),
            "land":"NL",
            "link":item["link"], 
            'sort_order':item['sort_order'],
            "beschrijving":" -- ".join(response.xpath('//article/div[@data-first_letter]//text()').getall()),
        }
