# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import json
import os
from itemadapter import ItemAdapter


class ScrapyVacaturesPipeline:

    def open_spider(self, spider):
        file_path = '/tmp/scrapy_' + spider.name +'.jl'
        if os.path.exists(file_path):
            os.remove(file_path)
        self.file = open(file_path, 'w')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        i = ItemAdapter(item).asdict()
        i['bron'] = spider.name
        line = json.dumps(i) + "\n"
        self.file.write(line)
        return item
