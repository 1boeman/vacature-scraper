import mysql.connector as database
import dev_config as config
import json
import glob



cnx = database.connect(
    user=config.db['user'],
    password=config.db['password'],
    host='localhost',
    database=config.db['database'],
)



def main():
    delete_old()
    data = process_scraper_data()

def delete_old():
    cursor = cnx.cursor(dictionary=True)
    sql = 'select id_vacature from vacature where DATE(datum_gevonden) <= CURDATE() - interval 2 day'
    cursor.execute(sql)
    ids = cursor.fetchall()
    delete_queries = [
      "delete from vacature_has_plaats where id_vacature = %s",
      "delete from vacature_has_vakgebied where id_vacature = %s",
      "delete from vacature where id_vacature = %s"]

    for v_id in ids:
        for sql in delete_queries:
            cursor.execute(sql,(v_id['id_vacature'],))

    cnx.commit()
    cursor.close()



def process_scraper_data():
    path = config.scrape_files['path']
    prefix = config.scrape_files['prefix']
    files = glob.glob(path + '/' + prefix + '*.jl')
    for f in files:
        h = open(f,'r')
        for line in h:
            data = json.loads(line)
            db_insert(data)
        h.close()
    cnx.close()



def db_insert(data):
    cursor = cnx.cursor(dictionary=True)
    

    data['titel'] = data['titel'][:1023]
    if (data['organisatie']):
        data['organisatie'] = data['organisatie'][:1023]
   
    if not data['plaatsnaam']:
        data['plaatsnaam'] = 'Plaatsnaam onbekend'

    # organisatie
    if 'organisatie' not in data or not data['organisatie'] or not len(data['organisatie'].strip()):
        data['organisatie'] = 'organisatie onbekend'

    query = ('select * from vacature_db.organisatie'
                ' where titel  = %s')
    cursor.execute(query, (data['organisatie'],))
    rows = cursor.fetchall()
 
    if cursor.rowcount == 0:
        # insert new if not in db 
        sql = ('INSERT INTO vacature_db.organisatie (titel) '
            ' VALUES (%s)') 
        cursor.execute(sql,(data['organisatie'],)) 
        data['id_organisatie'] = cursor.lastrowid
    else:
        data['id_organisatie'] = rows[0]['id_organisatie']

    #vacature
    query  = 'select * from vacature_db.vacature where titel = %s and link = %s'
    cursor.execute(query, (data['titel'].strip(),data['link'].strip(),))
    rows = cursor.fetchall()

    fields = ('titel','beschrijving','datum_pub','datum_sluit','salaris_indicatie','salaris_min','salaris_max','omvang',
                    'link','id_organisatie','opleiding','bron','sort_order',)
    values = []
    for f in fields:
        if f not in data or not data[f] or not len(str(data[f]).strip()):
            data[f] = None
        else:
            data[f] = str(data[f]).strip()
        values.append(data[f])
    if cursor.rowcount == 0:
        #insert
        sql = ('insert into vacature_db.vacature (' + ','.join(fields) + ',datum_gevonden)'
                      ' values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,NOW())')
        cursor.execute(sql, values)
        data['id_vacature'] = cursor.lastrowid
        
    else:
        #update
        data['id_vacature'] = str(rows[0]['id_vacature'])
        sql = 'update vacature_db.vacature set'
        set_clause = []
        for k in fields:
            set_clause.append( ' `' + k + '` = %s')  
        sql += ', '.join(set_clause)
        sql += ' where id_vacature = ' + str(rows[0]['id_vacature'])
        print (sql)
        cursor.execute(sql, values)
    
    # plaats
    query = ('select * from vacature_db.plaats '
                ' where plaatsnaam = %s and land = %s')
    print (data['plaatsnaam'])
    cursor.execute(query, (data['plaatsnaam'], data['land']))
    rows = cursor.fetchall()
    
    if cursor.rowcount == 0:
        # insert new place if place not in db 
        sql = ('INSERT INTO vacature_db.plaats (plaatsnaam, land) '
            ' VALUES (%s, %s) ') 
        cursor.execute(sql,(data['plaatsnaam'],data['land'],)) 
        data['id_plaats'] = cursor.lastrowid

    

    else:
        data['id_plaats'] = rows[0]['id_plaats']
    

    #koppel
    sql = ('insert ignore into vacature_has_plaats (id_vacature,id_plaats) values (%s,%s)')
    cursor.execute(sql,(data['id_vacature'],data['id_plaats']))
    # vakgebied
    if 'vakgebied' in data and data['vakgebied'] and len(data['vakgebied'].strip()):
        query = ('select * from vacature_db.vakgebied '
                    ' where titel = %s')
            
        cursor.execute(query, (data['vakgebied'],))
        rows = cursor.fetchall()
     
        if cursor.rowcount == 0:
            # insert new if not in db 
            sql = ('INSERT INTO vacature_db.vakgebied (titel) '
                ' VALUES (%s)') 
            cursor.execute(sql,(data['vakgebied'],)) 
            data['id_vakgebied'] = cursor.lastrowid

   
        else:
            data['id_vakgebied'] = rows[0]['id_vakgebied']

        #koppel
        sql = ('insert ignore into vacature_has_vakgebied (id_vacature,id_vakgebied) values (%s,%s)')
        cursor.execute(sql,(data['id_vacature'],data['id_vakgebied']))


    cnx.commit()
    cursor.close()

if __name__ == "__main__":
    main()
